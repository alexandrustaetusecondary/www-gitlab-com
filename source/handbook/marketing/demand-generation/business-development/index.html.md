---
layout: markdown_page
title: "Business Development"
---
Business Development Handbook

## On this page

### [Welcome](#welcome)

- [Your Role](#Role)
- [Onboarding](#onboard)

### [Process/Workflow](#bdring)

- [Inbound Process](#Inbound)
- [Outbound Process](#Outbound)
- [BDR & Team Lead](#TeamLead)
- [Researching](#Researching)
- [Prospecting 101](#Basic)
- [Email Guide](#emailGuide)
   - [Email Prospecting](#emailProspect)
- [Calling Guide](#callGuide)
   - [Call Prospecting](#callProspect)
   - [Cold Calls](#coldCalls)
   - [Discovery Calls](#discCalls)
- [Qualifying](#qualify)
- [Lead Management](#leadManage)
- [How to Videos/Tutorials](#HowTos)
- [Staying Organized](#organized)

### [Misc](#misc)

- [FAQ's](#questions)
- [Improvements/Contributing](#contribute)
- [Variable Compensation Guidelines](#compensation)
- [Vacation Days](#vacation)

### Welcome to the team!<a name="welcome"></a>

Welcome to GitLab and congratulations on landing a job with the best open source tech company, GitLab. We are excited to have you join the team! We look forward to working closely with you and seeing you grow and develop into a top performing salesperson.

On the BDR team we work hard, but have fun too. We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR can come with what seems as long days, hard work, frustration and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. security. GitLab is a place with endless opportunity. Let’s make it happen!

#### Your Role<a name="Role"></a>

As a Business Development Representative, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating qualified opportunities for GitLab as you look for leads, research companies, industries and different roles.

As you gain knowledge, you will be able to aid these key players in solving problems within the developer lifecycle. There are numerous resources at your fingertips that we have created to help you in this process. You have:

1. [Onboarding Issue](https://dev.gitlab.org) - This will typically fill up the majority of your first week. This is a step by step guide and checklist to getting everything in your arsenal set up like equipment, gmail, calendly, slack, security, and your Gitlab.com account. These todo’s provide you with the fundamentals.
2. [BDR Handbook](https://about.gitlab.com/handbook/marketing/demand-generation/business-development/) - This will help you with your day to day workflow. You can find information on how to prospect, best practices, customer FAQs, buyer types, cadence samples and more. Use the BDR handbook in conjunction with the [marketing](https://about.gitlab.com/handbook/marketing/) and [sales](https://about.gitlab.com/handbook/sales/) handbooks. This will help you bridge the gap between the two and learn the product and process faster.
3. [GLU GitLab University](https://university.gitlab.com/) - These trainings will teach the fundamentals of Version Control with Git and GitLab through courses that cover topics which can be mastered in about 2 hours. These trainings include an intro to Git, GitLab basics, a demo of Gitlab.com, terminology and more. You can also find information about GitLab compared to other tools in the market.

The rest of this guide will outline your onboarding at GitLab. Do not take this to be concrete. We hire very talented individuals who take initiative and advantage of the opportunities and situations presented to them. Be creative, learn and try different ways of doing things. We are excited to have you and cannot wait to see what you bring to the team!

#### Onboarding<a name="onboard"></a>

The following onboarding [issue template](https://https://about.gitlab.com/handbook/marketing/demand-generation/business-development/outbound-onboarding-checklist.html.md) will help bring you up to speed with your role as a BDR.

### Process/workflow<a name="bdring"></a>

#### Coined Terms

 - #BDRing - Doing your job...Kris Touzel
 - #SQLing - Winning at your job...Ryan Caplin
 - #BDR - Boss Destroying Revenue...Joe Lucas

#### Inbound Process<a name="Inbound"></a>

Each rep will be placed into the Marketo Queue and will receive a high volume of leads to work on a monthly and quarterly basis. Criteria for those leads are set by Marketo and the Director of Demand Generation

- Primary Responsibilities:
   - Manage and help inbound requests to community@gitlab.com and sales@gitlab.com
   - Strategize to develop the proper qualifying questions for all types of customers.
   - Be able to identify where a customer is in the sale and marketing funnel and take the appropriate action
   - Generate Sales Qualified Leads (SQLs)
- Rules of engagement
  - Inbound to work off of leads within SFDC
  - Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR.

#### Outbound Process<a name="Outbound"></a>

Each rep will be assigned a list of 15-30 targeted accounts to work on a monthly or quarterly basis.

Criteria for those accounts (still TBD):

- Tier
- Industry
- Number of Employees
- Revenue
- Populated with contacts and activity
- Last activity date
- Lost opportunities
- Former or current customers

Help Marketing create campaigns focusing on:

- Accounts using specific competitors
- Up-to-date Titles
- Up-to-date company sizes
- Contract end dates

Rules of engagement

- Outbound to work off of contacts and leads within SFDC
- Outbound does not touch any historical lead that has activity on it within the last 45 days. We are on the same team as inbound, no bad blood, etc.

#### BDR & BDR Team Lead<a name="TeamLead">

Formal Weekly 1:1

- Mental check-in (winning and success)
- Discuss progress on targeted accounts
- Coaching - email strategy, campaigns, cadence, best practices
- Review goals at the account level and personal level
- Strategy for next week
- Upcoming events/campaigns that can be leveraged
- Personal goals and commitments

#### Research Process<a name="Researching"></a>

3 X 3 X 3

Three things in three minutes about the:

- Organization
- Industry
- Prospect/Lead

Where to Start?

1. Salesforce
   - [Report](https://na34.salesforce.com/00O61000003JOUq) of prospects and customers with 500+ seat potential.
      - Check with Account Owner on how they would like you to proceed with the account.
      - If a customer, also check [Customer Success Issue Tracker](https://gitlab.com/gitlab-com/customer-success/issues) to see if an account expansion plan exists. If one does, please collaborate and contribute in the issue.
   - [Lost deals](https://na34.salesforce.com/00O610000017SDD), users and customers (CE and EE)
   - Contacts
      - What relevant titles should we be reaching out to
   - Activity History
      - Who in the org have we called before that we can follow up with or reference
   - Cross reference with [version.gitlab.com](https://version.gitlab.com/users/sign_in) to see their version and available other usage info
2. Linkedin
   - Summary/Bio - Anything mentioned about job responsibilities or skills
   - Previous work - Any Gitlab Customers that we can reference
   - Connections - Are they connected to anyone at GitLab for a possible introduction
3. Website
   - Mission Statement
   - Press Releases/Newsroom
   - (ctrl F) search for keywords

#### Buyer Types

Technical Buyer

   - (End User)
   - Collaborate, features, data

Functional Buyer

   - (Director)
   - Making team projects more collaborative

Economic Buyer

   - (C-Level, VP)
   - Hit Team Targets and ROI

Your messaging for each one will be different. An end user or lower level developer will not care much about the ROI on our product.

### Email Guide<a name="emailGuide"></a>

#### Email Prospecting<a name="emailProspect"></a>

#### Emails

- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

#### Email Components

- Subject line
- Preview Pain
- Opening Stanza (Best Practice is something about them)
- Benefit & Value Proposition
- Create Urgency
- Definite Ask with available days and times

### Calling Guide<a name="callGuide"></a>

#### Basics of Call Prospecting<a name="callProspect"></a>

- Call about them, not you
- Be confident and passionate
- Aim for every role but focus on decision makers
- Prospect into multiple departments
- Ask for time
- Focus on the your end game
- Make it easy to say yes
- Obtain a commitment

#### Structure of a Prospecting Call

- Introduction
- Initial Benefit Statement
- Qualification... See the criteria above in the handbook
- Reason for taking the call (add value)
- Inform them about GitLab
- Commitments

#### Cold Call Framework<a name="coldCalls"></a>

    Introduction
    Hi. This is (Name) from GitLab

    Ask for Time
    “Do you have just a moment?”

    Question
    “Have you heard of GitLab before?”

    Initial Message with IBS
    “GitLab helps (Title) in the (Industry) industry solve issues around (pain & need) or
    “(Industry specific client) used GitLab to (Case study value proposition)

    Confirm that they are the right person
    “I understand you are in charge of application/program development, is that correct?

    Schedule meeting
    "Do you have 15 minutes on (specific day and time) to discuss your (example - developer life cycle)?"

    Initial Benefit Statement example

       - GitLab is an end-to-end developer lifecycle solution that allows developers to collaborate on code, project management, and to create a seamless workflow between teams across the entire org.

### Qualifying<a name="qualify"></a>

[Qualification Criteria](https://docs.google.com/document/d/1GR0v4cvBYUoTSn66kVAqzY5q_imWbDWW4ifo7ag6Gow/edit)

### Lead Management<a name="leadManage"></a>

How do I know who to assign a lead to?

- [Here](https://docs.google.com/document/d/1Zh63p4PL5LCyneW_Jpewv-gGXTkAleaklSJ7z3spGLw/edit?ts=57e98e6b)

How do I convert a lead to an opportunity?

- [Here](https://docs.google.com/document/d/1Txt9_ErefZpoPMSm6tCp5tZhO6am7qv3fXzrZo84K3k/edit)

How do I access the handbook?

- [Here](https://about.gitlab.com/primer/)

How do I deal with APAC and SA leads?

- [Here](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit)
   - Note: BDRs do not set up meetings for resellers.

### Miscellaneous<a name="misc"></a>

#### Frequently Asked Questions<a name="questions"></a>

How do I get help/assistance?

- where to ask questions, what to do before and after

I’m having issues with Salesforce (can’t login, my leads are not syncing, salesforce isn't working, I want to request more contacts from discoverorg, not sure what the protocol is, etc.)

- Slack - Francis Aquino

My GitLab account isn’t working. I can’t seem to login or access anything

- Slack - Assigned Buddy

What are my work hours?

- The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”

[Additional FAQ's](https://docs.google.com/a/gitlab.com/document/d/1O_DTqnsQaHpqFeA7h24R-ozu0ZeyHey0yNI0GIAA7_w/edit?usp=sharing)

#### What links should I be familiar with?

 [Trinet](https://sso.trinet.com/) | [Salesforce](https://login.salesforce.com/) |[Slack](https://gitlab.slack.com) | [Expensify](https://www.expensify.com) |[Bamboohr](https://gitlab.bamboohr.com) | [Calendly](https://calendly.com/) | [Collabspot](https://go.collabspot.com) | [GitLab Version Check](https://version.gitlab.com)

#### What Slack channels should I join?
  #bdr-team | #general | #marketing | #peopleops | #questions | #random | #sales | #sfdc-users | #support | #thanks | #wins | #working-on

#### Who should I connect with for my 10 1:1 onboarding calls?

- You can connect with whomever you please. However, you will interact the most with the Sales Team and the Demand Generation Team. * See the [GitLab Team Page](https://about.gitlab.com/team/) for suggestions.

#### Improvements/Contributing<a name="contribute"></a>

using issues, starting projects

#### Variable Compensation Guidelines<a name="compensation"></a>

Full-time BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on results. Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

Guidelines for bonuses:

- Team and individual objectives are based on GitLab's revenue targets and adjusted monthly.
- Bonuses are paid quarterly.
- Bonuses may be based on various objectives
   - E.g. a new BDR's first month's bonus is typically based on completing [onboarding](https://about.gitlab.com/handbook/general-onboarding/)
   - Bonuses may be tied to sales target attainment, [OKRs](https://about.gitlab.com/handbook/marketing/#okrs), or other objectives.

#### BDR Vacation Incentives<a name="vacation"></a>

Note: this is a new, experimental policy started in June 2016: changes and adjustments are expected

At GitLab, we have an [unlimited time off policy](https://about.gitlab.com/handbook/#paid-time-off). The business development team has additional incentives for taking time off:

- The first 5 business days taken off by a BDR in a calendar year prorates their target by 100%, rounded up (i.e. reduces the BDR's target by the period's target divided by the number of business days in the period, times the number of days taken off).
- The second 5 business days are prorated by 50%, rounded up if applicable
- The third 5 business days are prorated by 25%, rounded up if applicable
- Example: A BDR with a target of 200 opportunities in a January with 20 business days can reduce that month's target to 190 by taking 1 day off, 150 by taking 5 days off, 125 by taking 10 days off, or 113 by taking 15 days off.

Rules and qualifications for BDR Vacation Incentives:

- BDRs must give notice 30 days ahead of intended time off to by the Team Lead and Senior Demand Generation Manager (and ensure they acknowledge receipt within 48 hours)
- BDRs must add intended time off to the availability calendar
- Prorated days can be deferred (i.e. take PTO without prorating the target), but are lost at the end of the year. Incidentally, the math usually works out in favor of not deferring.
- BDRs can take additional vacation days (see [GitLab PTO policy](https://about.gitlab.com/handbook/#paid-time-off), but time off beyond 15 days will not adjust targets.

With [GitLab's PTO policy](https://about.gitlab.com/handbook/#paid-time-off), there's no need to give notice or ask permission to take time off. Unfortunately, a quota carrying BDR can expect to lose some earning potential for taking PTO, which often leads to BDRs not taking vacation. Another problem with unlimited PTO is that management loses forecasting power when BDRs take unexpected PTO, making it tempting for leadership to discourage vacation. We hope that this additional vacation incentive for the team acts as a balancing incentive to both parties by (a) providing a prorated quota for BDRs who plan ahead and (b) helping leadership plan and forecast better without discouraging vacation.

#### Additional Resources

- [GitLab University](https://university.gitlab.com/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab CI](https://about.gitlab.com/2016/07/22/building-our-web-app-on-gitlab-ci/)
- [GitLab Editions](https://about.gitlab.com/features/#compare)
- [GitLab compared to other tools](https://about.gitlab.com/comparison/)
