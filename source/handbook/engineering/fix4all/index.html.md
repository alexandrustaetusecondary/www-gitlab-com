---
layout: markdown_page
title: "fix4all: Developer Rotation on the Support Team"
---

## Background

The support team fields a wide array of questions from customers, and
we as the company learn quite a lot about ways to improve GitLab just
by helping our customers. In addition, sometimes customer-reported
bugs get addressed in a timely manner, but other times they remain
unfixed for quite some time.

## Introducing fix4all

fix4all is a rotation where one developer--or anyone in GitLab--will
spend a week of his/her time helping the support team. There are
several goals:

1. Reduce the time it takes to resolve customer tickets.
1. Reduce the time to fix customer bugs.
1. Increase the shared knowledge between developers and service engineers.
1. Increase customer awareness within the development team.
1. Foster better cooperation between members of different groups.

## Implementation

At the release kickoff, we will assign four developers to
be on the fix4all rotation. Each participant will be assigned a week
within that month to focus on helping support.

### Tasks

Each developer will be assigned a week where they work directly with
the support team. The exact day-to-day tasks may vary depending on the
most pressing needs at the moment. See the [Support
Handbook](https://about.gitlab.com/handbook/support/) for more
details.

### Bugs

If customer ticket load appears under control, a person on the fix4all
rotation should fix customer bugs, starting with the highest priority
items in the [GitLab
CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=customer&sort=priority)
and [EE issue
trackers](https://gitlab.com/gitlab-org/gitlab-ee/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=customer&sort=priority).
